from time import sleep
import jsons

def extract_image(response):
    response  = jsons.loads(response)
    if response['status'] != 200:
        return False
    with open("screenshot.html", "w") as outfile:
        outfile.write('<img src="' + response['outputImage'] + '">')

