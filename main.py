import sys, time
from dab_client import DabClient
import output

class Pause:
    def __init__(self, duration = 3):
        self.duration = duration

class Demo:
    STEPS = [
        ("version", "{}"),
        ("output/image", "{}"),
        ("applications/launch", '{"appId": "YouTube"}'),
        # ("input/key-press", '{"keyCode": "KEY_ENTER"}'),
        ("operations/list", "{}"),
        ("version", "{}"),
        #Pause(), # Running Youtube
        # Open Collabora Channel
        ("input/key-press", '{"keyCode": "KEY_LEFT"}'),
        ("input/key-press", '{"keyCode": "KEY_DOWN"}'),
        ("input/key-press", '{"keyCode": "KEY_DOWN"}'),
        ("input/key-press", '{"keyCode": "KEY_DOWN"}'),
        ("input/key-press", '{"keyCode": "KEY_DOWN"}'),
        ("input/key-press", '{"keyCode": "KEY_RIGHT"}'),
        ("input/key-press", '{"keyCode": "KEY_DOWN"}'),
        ("input/key-press", '{"keyCode": "KEY_RIGHT"}'),
        ("output/image", "{}"),
        ("input/key-press", '{"keyCode": "KEY_UP"}'),
        ("input/key-press", '{"keyCode": "KEY_RIGHT"}'),
        ("input/key-press", '{"keyCode": "KEY_ENTER"}'),
        Pause(0.5),
        ("output/image", "{}"),

        # Navigate Collabora channel
        ("input/key-press", '{"keyCode": "KEY_DOWN"}'),
        ("input/key-press", '{"keyCode": "KEY_RIGHT"}'),
        ("input/key-press", '{"keyCode": "KEY_DOWN"}'),
        ("input/key-press", '{"keyCode": "KEY_DOWN"}'),
        ("input/key-press", '{"keyCode": "KEY_DOWN"}'),
        ("output/image", "{}"),
        ("input/key-press", '{"keyCode": "KEY_RIGHT"}'),
        ("input/key-press", '{"keyCode": "KEY_RIGHT"}'),
        ("input/key-press", '{"keyCode": "KEY_DOWN"}'),
        ("input/key-press", '{"keyCode": "KEY_UP"}'),
        ("applications/get-state", '{"appId": "YouTube"}'),
        ("operations/list", "{}"),
        ("input/key-press", '{"keyCode": "KEY_RIGHT"}'),
        ("input/key-press", '{"keyCode": "KEY_RIGH"}'),
        #("input/key-press", '{"keyCode": "KEY_UP"}'),
        #("input/key-press", '{"keyCode": "KEY_UP"}'),
        #("input/key-press", '{"keyCode": "KEY_UP"}'),
        ("output/image", "{}"),
        #("input/key-press", '{"keyCode": "KEY_BACK"}'),
        #("input/key-press", '{"keyCode": "KEY_BACK"}'),
        #Pause(0.5),
        #("input/key-press", '{"keyCode": "KEY_DOWN"}'),
        #("input/key-press", '{"keyCode": "KEY_ENTER"}'),
        #Pause(1), #Quit YouTube
        ("applications/exit", '{"appId": "YouTube"}'),

        ("output/image", "{}"),
        ("device/info", "{}"),
        ("version", "{}"),
        ("input/key/list", "{}"),
        #Pause(),
        ("output/image", "{}"),
        ("input/key-press", '{"keyCode": "KEY_DOWN"}'),
        ("input/key-press", '{"keyCode": "KEY_DOWN"}'),
        ("input/key-press", '{"keyCode": "KEY_RIGHT"}'),
        ("input/key-press", '{"keyCode": "KEY_RIGHT"}'),
        ("input/key-press", '{"keyCode": "KEY_LEFT"}'),
        ("input/key-press", '{"keyCode": "KEY_LEFT"}'),
        ("input/key-press", '{"keyCode": "KEY_UP"}'),
        ("input/key-press", '{"keyCode": "KEY_UP"}'),
    ]

    def __init__(self, broker, device_id):
        self.dab_client = DabClient()
        self.dab_client.connect(broker, 1883)
        self.device_id = device_id

    def execute_cmd(self, dab_request_topic, dab_request_body="{}"):
        print("Running: %s : %s" % (dab_request_topic, dab_request_body))
        self.dab_client.request(
            self.device_id, dab_request_topic, dab_request_body)
        if self.dab_client.last_error_code() == 200:
            return 0
        else:
            return 1

    def execute_line(self, line):
        if isinstance(line, Pause):
            time.sleep(line.duration)
            return
        (dab_request_topic, dab_request_body) = line
        code = self.execute_cmd(dab_request_topic, dab_request_body)
        response = self.dab_client.response()
        if dab_request_topic == "output/image":
            output.extract_image(response)
        else:
            print(response)
        time.sleep(0.3) # For dramatic effect

    def run_all(self):
        while True:
            for line in self.STEPS:
                self.execute_line(line)


if __name__ == "__main__":

    broker = sys.argv[1]

    if len(sys.argv) == 2:
        dab_client = DabClient()
        dab_client.connect(broker, 1883)
        dab_client.request_discovery()
        response = dab_client.response()
        print(response)
    else:

        device_id = sys.argv[2]
        demo = Demo(broker, device_id)
        #demo.execute_line(('applications/exit','{"appId": "YouTube"}'))
        #demo.execute_line(('applications/launch-with-content','{"appId": "YouTube", "contentId": "@Collabora"}'))
        demo.run_all()
