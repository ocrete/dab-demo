# IBC 2023 DAB Demo #

This project contains an automated demo for DAB prepared for IBC 2023.

## Prerequisite ##
Python minimal version 3.8

Please edit config.py to have the device app line up with your system settings.

```
pip3 install -r requirements.txt
```


## Running ##

Run with

```
python3 main.py <IP address of device> <device ID>
```